<?php
/**
 * @file
 * tt_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function tt_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-new-time-entry_new-time-entry:node/add/time-entry
  $menu_links['menu-new-time-entry_new-time-entry:node/add/time-entry'] = array(
    'menu_name' => 'menu-new-time-entry',
    'link_path' => 'node/add/time-entry',
    'router_path' => 'node/add/time-entry',
    'link_title' => 'New time entry',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-new-time-entry_new-time-entry:node/add/time-entry',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('New time entry');


  return $menu_links;
}
