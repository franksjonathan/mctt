<?php
/**
 * @file
 * mctt_panels.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function mctt_panels_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home';
  $page->task = 'page';
  $page->admin_title = 'Home';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home__panel_context_d731a556-32fa-40bd-932f-5c9edd2550b1';
  $handler->task = 'page';
  $handler->subtask = 'home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6be1c325-2cfa-4951-af02-4801cf7f0508';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-efd38aae-f938-4a4b-b3f5-88d7f4345d25';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-new-time-entry';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'efd38aae-f938-4a4b-b3f5-88d7f4345d25';
    $display->content['new-efd38aae-f938-4a4b-b3f5-88d7f4345d25'] = $pane;
    $display->panels['middle'][0] = 'new-efd38aae-f938-4a4b-b3f5-88d7f4345d25';
    $pane = new stdClass();
    $pane->pid = 'new-5a10c08f-db0b-4874-b468-2ba73913a038';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'my_month_of_entries';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5a10c08f-db0b-4874-b468-2ba73913a038';
    $display->content['new-5a10c08f-db0b-4874-b468-2ba73913a038'] = $pane;
    $display->panels['middle'][1] = 'new-5a10c08f-db0b-4874-b468-2ba73913a038';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-efd38aae-f938-4a4b-b3f5-88d7f4345d25';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home'] = $page;

  return $pages;

}
