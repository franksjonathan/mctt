<?php
/**
 * @file
 * _node_time_entry.features.inc
 */

/**
 * Implements hook_node_info().
 */
function _node_time_entry_node_info() {
  $items = array(
    'time_entry' => array(
      'name' => t('Time Entry'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
