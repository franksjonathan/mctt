<?php
/**
 * @file
 * _views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function _views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
