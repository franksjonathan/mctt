@api @client
Feature: TT3 See all entries for a client on the client's term page
  As a user,
  I want to see all of the time entries for a specific client
  so that I can see how much work we've done for that client.

  Scenario: No entries - empty client page
    Given "Client" terms:
      | name             |
      | TT3 First Client |
    When I am viewing the "TT3 First Client" term
    Then I should see the text "There is currently no content classified with this term."

  Scenario: Time entry should appear on client term page
    Given "Client" terms:
      | name              |
      | TT3 First Client  |
      | TT3 Second Client |
    And "Project" terms:
      | name          |
      | Project TT3-1 |
    And "Position" terms:
      | name       |
      | TT3 Tester |
    And "Time Entry" nodes:
      | title       | field_time | field_project | field_date  | field_position | field_client     |
      | TT3 Entry 1 | 5          | Project TT3-1 | @today_date | TT3 Tester     | TT3 First Client |
    When I am viewing the "TT3 First Client" term
    Then I should not see the text "There is currently no content classified with this term."
    But I should see the link "TT3 Entry 1"