@api @project
Feature: TT4 Project page shows entries for that project
  As a user,
  I want to view all of the time entries for a specific project
  so that I can see how much work we have logged for that project.

  Background:
    Given "Project" terms:
      | name              |
      | TT4 First Project |

  Scenario: No entries - empty client page
    When I am viewing the "TT4 First Project" term
    Then I should see the text "There is currently no content classified with this term."

  Scenario: Time entry should appear on project term page
    Given "Client" terms:
      | name              |
      | TT4 First Client  |
      | TT4 Second Client |
    And "Project" terms:
      | name          |
      | Project TT4-1 |
    And "Position" terms:
      | name       |
      | TT4 Tester |
    And "Time Entry" nodes:
      | title       | field_time | field_project | field_date  | field_position | field_client     |
      | TT4 Entry 1 | 5          | Project TT4-1 | @today_date | TT4 Tester     | TT4 First Client |
    When I am viewing the "Project TT4-1" term
    Then I should not see the text "There is currently no content classified with this term."
    But I should see the link "TT4 Entry 1"

