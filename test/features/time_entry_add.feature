@api @timeentry
Feature: TT-1
  As a worker,
  I want to enter the time I spend on a project in a day
  so that we can bill our clients appropriately.

  Scenario: Date should default to today's date
    Given I am logged in as a user with the "authenticated user" role
    When I click "New time entry"
    And I should see the text "@currentmonth_short"
    And I should see the text "@currentday"
    Then I fill in "A title" for "Title"
    Then "@currentmonth_short" in "Month" should be selected
    Then "@currentday" in "Day" should be selected
    Then "@currentyear" in "Year" should be selected

  Scenario: Projects dropdown should contain all of the projects
    Given I am logged in as a user with the "authenticated user" role
    And "Project" terms:
      | name           |
      | TT-1 Project A |
      | TT-1 Project B |
      | TT-1 Project C |
    When I click "New time entry"
    Then I select "TT-1 Project A" from "Project"
    Then I select "TT-1 Project B" from "Project"
    Then I select "TT-1 Project C" from "Project"

  Scenario: Form validation
    Given I am logged in as a user with the "authenticated user" role
    When I click "New time entry"
    And I press "Save"
    Then I should see the error message containing "Title field is required."
    And I should see the error message containing "Project field is required."
    And I should see the error message containing "Time field is required."
    And I should see the error message containing "Client field is required."
    And I should see the error message containing "Position field is required."
    And I fill in "Time" with "A"
    And I press "Save"
    Then I should see the error message containing "Only numbers and the decimal separator (.) allowed in Time."

  Scenario: Save successful entry
    Given I am logged in as a user with the "authenticated user" role
    And "Project" terms:
      | name           |
      | TT-1 Project A |
    And "Position" terms:
      | name      |
      | Developer |
    And "Client" terms:
      | name    |
      | SuperCo |
    When I click "New time entry"
    Then I select "TT-1 Project A" from "Project"
    And I fill in "Title" with "TT1 Time Entry @timestamp"
    And I fill in "Body" with "What I did today"
    And I fill in "Time" with "8"
    And I select "Developer" from "Position"
    And I select "SuperCo" from "Client"
    And I press "Save"
    Then I should see the message containing "Time Entry TT1 Time Entry @timestamp has been created."