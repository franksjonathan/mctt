@api @calendar
Feature: TT2 Homepage Calendar

  Scenario: Homepage calendar is month view
    Given I am logged in as a user with the "authenticated user" role
    And I am on the homepage
    Then I should see the text "Sun"
    Then I should see the text "Mon"
    Then I should see the text "Tue"
    Then I should see the text "Wed"
    Then I should see the text "Thu"
    Then I should see the text "Fri"
    Then I should see the text "Sat"

  Scenario: Adding an entry should appear on the calendar
    Given I am logged in as a user with the "authenticated user" role
    And I have no "Time Entry" nodes
    And I am on the homepage
    Then I should not see the link "Entry 1"
    And I should not see the link "Entry 2"
    And "Project" terms:
      | name          |
      | Project TT2-1 |
      | Project TT2-2 |
    And "Client" terms:
      | name     |
      | Client 1 |
      | Client 2 |
    And "Position" terms:
      | name             |
      | Tester           |
      | Developer        |
      | Business Analyst |
    And "Time Entry" nodes:
      | title   | uid         | field_time | field_project | field_date     | field_position | field_client |
      | Entry 1 | @currentuid | 5          | Project TT2-1 | @today_date    | Developer      | Client 1     |
      | Entry 2 | @currentuid | 3          | Project TT2-2 | @today_date    | Tester         | Client 2     |
      | Entry 3 | @currentuid | 5          | Project TT2-1 | @tomorrow_date | Developer      | Client 1     |
      | Entry 4 | @currentuid | 3          | Project TT2-2 | @tomorrow_date | Tester         | Client 2     |
    When I am on the homepage
    Then I should see the link "Entry 1"
    And I should see the link "Entry 2"
    And I should see the link "Entry 3"
    And I should see the link "Entry 4"
    And "Entry 1" should precede "Entry 2" for the query "#my_month_of_entries-@today_date-0 a"
    And "Entry 3" should precede "Entry 4" for the query "#my_month_of_entries-@tomorrow_date-0 a"