<?php

use Behat\MinkExtension\Context\RawMinkContext,
  Behat\Behat\Hook\Scope\AfterScenarioScope;

/**
 * Defines application features from the specific context.
 */
class VariableContext extends RawMinkContext implements SnippetAcceptingInterface {
  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * @Then /^the site name should be "([^"]*)"$/
   */
  public function theSiteNameShouldBe($arg1) {
    $site_name = variable_get('site_name', '');
    if ($site_name !== $arg1) {
      throw new Exception("The site name should be $arg1 but is actually $site_name.");
    }
  }

}