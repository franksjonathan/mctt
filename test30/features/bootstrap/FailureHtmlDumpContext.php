<?php

use Behat\MinkExtension\Context\RawMinkContext,
  Behat\Behat\Hook\Scope\AfterScenarioScope;

/**
 * Defines application features from the specific context.
 */
class FailureHtmlDumpContext extends RawMinkContext {
  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * @AfterScenario
   */
  public function takeScreenshotAfterFailedStep(AfterScenarioScope $scope) {
    if (99 === $scope->getTestResult()->getResultCode()) {
      $filename = $scope->getFeature()->getFile() . '-' . $scope->getScenario()
          ->getLine();
      $this->takeScreenshot($filename);
    }
  }

  /**
   * @Then /^I take a screenshot$/
   */
  public function iTakeAScreenshot() {
    $dir = explode('/', __DIR__);
    array_pop($dir); // Remove bootstrap
    array_pop($dir); // And features
    $dir[] = 'tests';
    $dir[] = 'shot-0';
    $file_name = implode('/', $dir);

    $this->takeScreenshot($file_name);
  }


  private function takeScreenshot($scenarioFileName) {
    $filePieces = explode('/', $scenarioFileName);

    $fileName = array_pop($filePieces) . '-' . time() . '.html';
    array_pop($filePieces);

    $html_dump_path = implode('/', $filePieces) . '/failures';
    if (!file_exists($html_dump_path)) {
      mkdir($html_dump_path);
    }

    $html = $this->getSession()->getPage()->getContent();
    $htmlCapturePath = $html_dump_path . '/' . $fileName;
    file_put_contents($htmlCapturePath, $html);

    $message = "\nHTML available at: " . $htmlCapturePath . ' (' . strlen($html) . ')';
    print $message . PHP_EOL;
  }
}