@api
Feature: The site should have current information on it

  Scenario: Site title on page
    Given I am an anonymous user
    And I am on the homepage
    Then I should see the text "Twin Cities Drupal Camp Time Tracker"

  Scenario: Use the API
    Then the site name should be "Twin Cities Drupal Camp Time Tracker"